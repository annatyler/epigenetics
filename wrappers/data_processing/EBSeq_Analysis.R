

EBSeq_Analysis <- function(expr.table, conditions, maxround = 8){
	
	Sizes <- MedianNorm(expr.table)
	EBout <- EBTest(Data = expr.table, Conditions = conditions, sizeFactors = Sizes, maxround = maxround)

	# diff.exp = GetDEResults(EBout, FDR = 0.1)
	# sig.genes <- diff.exp$DEfound
	# num.sig <- length(sig.genes)
	# geneFC <- PostFC(EBout)
	# PlotPostVsRawFC(EBout, geneFC)

	# EBout$Alpha
	# EBout$Beta
	# EBout$P
	
	# par(mfrow = c(1,2));QQP(EBout)
	# par(mfrow = c(1,2));DenNHist(EBout)

	retrun(EBout)
	
	
	
}