#This script adds expression data from the additional
#strains to the 5StrainsEffCts.txt file.

orig.table <- read.table("~/Documents/Epigenetics/Data/RNASeq/5StrainsEffCts.txt", stringsAsFactors = FALSE, sep = "\t", header = TRUE)

setwd("~/Documents/Epigenetics/Data/RNASeq/additional_strain_read_counts")
all.files <- list.files()
sample.names <- unlist(lapply(strsplit(all.files, "\\."), function(x) x[1]))


new.expr <- matrix(NA, nrow = nrow(orig.table), ncol = length(all.files))
colnames(new.expr) <- sample.names

for(i in 1:length(all.files)){
	new.data <- read.table(all.files[i], stringsAsFactors = FALSE, sep = "\t", header = TRUE)
	print(identical(new.data[,1], rownames(orig.table)))
	new.expr[,i] <- 	new.data[,2]
	}
	
final.table <- cbind(orig.table, new.expr)
write.table(final.table, "~/Documents/Epigenetics/Data/RNASeq/9StrainsEffCts.txt", sep = "\t", quote = FALSE)