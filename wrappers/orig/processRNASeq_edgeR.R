library(biomaRt)
library(ChIPpeakAnno)
library(org.Mm.eg.db)
#get handle to biomaRT
ens=useEnsembl(biomart = "ensembl")
mm10=useEnsembl(biomart="ensembl", dataset="mmusculus_gene_ensembl")
#listAttributes(mm10)

#////////////////////functions//////////////////////////////
create_data_subsets=function(){
  # get big file with all the strains
  data=as.matrix(read.delim("5StrainsEffCts.txt", header=TRUE, sep="\t"))
  colnames(data)
  
  #create an annotation with sample names, strains, treatments, etc
  length(colnames(data))
  
  annot=data.frame(sample=colnames(data), strain=substr(colnames(data),1,2), treatment=substr(colnames(data),3,3))
  colors=c("red","gold","green","blue","purple")
  strains=unique(annot$strain)
  col=data.frame(colors=colors,strain=strains)
  annot <- merge(col,annot,by="strain",sort=F)
  save(annot,file="RNASeqAnnotation.RData")
  #load("RNASeqAnnotation.RData")
  
  #now lets create some definitions for subsets
  is.cx=which(annot$treatment=="C")
  is.tx=which(annot$treatment=="T")
  is.b6=which(annot$strain=="B6")
  is.ca=which(annot$strain=="CA")
  is.db=which(annot$strain=="DB")
  is.pk=which(annot$strain=="PK")
  is.ws=which(annot$strain=="WS")
  
  #subset the data and save each subset as an R object
  all5C=data[,is.cx]
  save(all5C, file="all5C-effcountdata.RData")
  all5T=data[,is.tx]
  save(all5T, file="all5T-effcountdata.RData")
  b6=data[,is.b6]
  save(b6, file="B6-effcountdata.RData")
  ca=data[,is.ca]
  save(ca, file="CA-effcountdata.RData")
  db=data[,is.db]
  save(db, file="DB-effcountdata.RData")
  pk=data[,is.pk]
  save(pk, file="PK-effcountdata.RData")
  ws=data[,is.ws]
  save(ws, file="WS-effcountdata.RData")
  b6.ca=data[,c(is.b6,is.ca)]
  save(b6.ca, file="B6vsCA-effcountdata.RData")
  b6.db=data[,c(is.b6,is.db)]
  save(b6.db, file="B6vsDB-effcountdata.RData")
  b6.pk=data[,c(is.b6,is.pk)]
  save(b6.pk, file="B6vsPK-effcountdata.RData")
  b6.ws=data[,c(is.b6,is.ws)]
  save(b6.ws, file="B6vsWS-effcountdata.RData")
  ca.db=data[,c(is.ca,is.db)]
  save(ca.db, file="CAvsDB-effcountdata.RData")
  ca.pk=data[,c(is.ca,is.pk)]
  save(ca.pk, file="CAvsPK-effcountdata.RData")
  ca.ws=data[,c(is.ca,is.ws)]
  save(ca.ws, file="CAvsWS-effcountdata.RData")
  db.pk=data[,c(is.db,is.pk)]
  save(db.pk, file="DBvsPK-effcountdata.RData")
  db.ws=data[,c(is.db,is.ws)]
  save(db.ws, file="DBvsWS-effcountdata.RData")
  pk.ws=data[,c(is.pk,is.ws)]
  save(pk.ws, file="PKvsWS-effcountdata.RData")
}

# performs pairwise comparison of two groups
doEdgeRAnalysis= function(samples_in, x_in, group_in){
  #create the DGEList and save it
  y=DGEList(counts=x_in, group=group_in)
  fname=paste0(samples_in,"_Y.RData")
  save(y,file=fname)
  y$samples
  levels(y$samples$group)

  #filter, calc norm factors, run fit and qlf
  keep <- rowSums(cpm(y)>1) >= 2
  y <- y[keep, , keep.lib.sizes=FALSE]
  y <- calcNormFactors(y)
  design <- model.matrix(~group)
  y <- estimateDisp(y,design)
  fit <- glmQLFit(y,design)
  qlf <- glmQLFTest(fit,coef=2)
  #save qlf
  fname=paste0(samples_in,"_qlf.RData")
  save(qlf, file=fname)
  
  #get the top tags
  fname=paste0(samples_in, "_topTags.txt")
  (tt=as.data.frame(topTags(qlf)))
  write.table(tt, file=fname, sep=",", row.names = TRUE, col.names = TRUE, quote=FALSE)
  
  #lets get more than the top tags
  #filter qlf table for p<.05, abs(logfc>1)
  p05=which(qlf$table$PValue<0.5)
  fc2=which(abs(qlf$table$logFC)>1)
  p05fc2=intersect(p05,fc2)
  
  #create results outputs
  de=qlf$table[p05fc2,c("logFC","PValue")]
  
  #are the top ten in our de?
  (intersect(rownames(tt),rownames(de)))
  
  # get the ensembl ids for the de genes
  ensIds=rownames(de)
  
  # get chromosomal coordinates for the mapped genes 
  attr=c('ensembl_gene_id','external_gene_name', 'chromosome_name','start_position','end_position')
  chrlocs=getBM(attributes=attr, filters = "ensembl_gene_id", values=ensIds, mart=mm10)
  rownames(chrlocs)=chrlocs$ensembl_gene_id
  chrlocs=chrlocs[,-1]
  
  # merge this with the de results table and save
  de = merge.data.frame(chrlocs, de, by.x=0, by.y=0, all.x=TRUE, all.y=TRUE)
  colnames(de) = c("EnsemblID","Gene", "Chr","Start","End", "logFC", "PValue")
  fname=paste0(samples_in, "_qlftable_de.txt")
  write.table(de, file=fname, sep="\t", row.names = FALSE, col.names = TRUE, quote=FALSE)
  
  #get up and down regulated genes
  fname=paste0(samples_in, "_de_up.txt")
  de.up=de[which(de$logFC<0),]
  write.table(de.up, file=fname, sep="\t",  row.names = FALSE, col.names = TRUE, quote=FALSE)
  
  fname=paste0(samples_in, "_de_down.txt")
  de.down=de[which(de$logFC>0),]
  write.table(de.down, file=fname, sep="\t",  row.names = FALSE, col.names = TRUE, quote=FALSE)
  
  #get go enrichment data
  tt.names=tt[,1]
  getGOEnrichment(paste0(samples_in,"_TopTags"), tt.names)
  
  de.up.names=c(de.up[,1])
  getGOEnrichment(paste0(samples_in,"_up"), de.up.names)
  
  de.down.names=c(de.down[,1])
  getGOEnrichment(paste0(samples_in,"_down"), de.down.names)
  
}

# performs Anova like comparison of multiple groups
doEdgeRAnalysisMultiGroups= function(samples_in, x_in, group_in){
  #create the DGEList and save it
  y=DGEList(counts=x_in, group=group_in)
  fname=paste0(samples_in,"_Y.RData")
  y$samples
  levels(y$samples$group)
  # [1] "B6" "CA" "DB" "PK" "WS"
  
  #this part is specific to multiple groups
  #we are going to do an ANOVA-like analysis
  #to look at differences between any of the groups
  #create design matrix with an intercept term
  #intercept is the group against which all others are compared
  design <- model.matrix(~group, data=y$samples)
  (design)
  y <- estimateDisp(y, design)
  save(y,file=fname)
  
  fit <- glmFit(y, design)
  (fit)
  #now we are going to make comparisons between the groups
  #could specify comparison between intercept an individual groups
  #but we really want to compare them all to the intercept (B6)
  #coeff needs to specify all groups except the intercept
  lrt <- glmLRT(fit, coef=2:5)
  # colnames(lrt$design)
  # [1] "(Intercept)" "groupCA"     "groupDB"     "groupPK"     "groupWS" 
  # lrt$design
#   (Intercept) groupCA groupDB groupPK groupWS
#   WSC4           1       0       0       0       1
#   WSC3           1       0       0       0       1
#   WSC2           1       0       0       0       1
#   PKC3           1       0       0       1       0
#   PKC2           1       0       0       1       0
#   PKC1           1       0       0       1       0
#   DBC4           1       0       1       0       0
#   DBC3           1       0       1       0       0
#   DBC2           1       0       1       0       0
#   CAC5           1       1       0       0       0
#   CAC4           1       1       0       0       0
#   CAC1           1       1       0       0       0
#   B6C4           1       0       0       0       0
#   B6C2           1       0       0       0       0
#   B6C1           1       0       0       0       0
  
  
  save(lrt, file=paste0(samples,"_lrt.RData"))
  
  # get the top tags n=100 => top 100 tags
  tt=as.data.frame(topTags(lrt, n=100))
  fname=paste0(samples_in, "_topTags.csv")
  write.table(tt, file=fname, sep=",", row.names = TRUE, col.names = TRUE, quote=FALSE)
  
  #going to save the whole table in case we want to look at all the data
  fname=paste0(samples_in, "_lrt_table.csv")
  write.table(lrt$table, file=fname, sep=",", row.names = TRUE, col.names = TRUE, quote=FALSE)
}

getGOEnrichment = function (samples_in, ids_in){
  (samples_in)
  (ids_in)
  # get go enrichment data 
  go=getEnrichedGO(ids_in, orgAnn="org.Mm.eg.db", feature_id_type="ensembl_gene_id",
                   maxP=0.01, multiAdj=FALSE, minGOterm=10, multiAdjMethod="")
  #save it to data file
  fname=paste0(samples_in,"_GO.RData")
  save(go, file=fname)
  
  #do a quick print of all 3 GO categories
  fname=paste0(samples_in,"_GO_summary.txt")
  items=c(paste0("**************** ", samples, " ****************"),
          "///////////// BP terms //////////////", unique(go$bp$go.term), 
          "///////////// MF terms //////////////", unique(go$mf$go.term), 
          "///////////// CC terms //////////////", unique(go$cc$go.term))
  for(i in items){
    write.table(i,fname, append=TRUE, quote=FALSE, row.names=FALSE, col.names=FALSE)
  }
  
}

#/////////////////////////////////end functions //////////////////////////////

# files<-list.files(datadir, include.dirs = FALSE)
# (files)
# 
#  ctr<-0
#  for(f in files){
#    s<-strapplyc(f, "(.*)\\.genes.effective_read_counts", simplify = TRUE)
#    print(s)
#    df<-read.delim(file.path(datadir,f),header=TRUE,sep="\t")
#    df<-subset(df[,c(1,2)])
#    colnames(df)<-c("locus",s)
#    if(ctr==0) {
#      merged<-df
# #     #print(merged)
#      ctr<-1 
#    }else{
#      merged<-merge(df,merged,by="locus")
#    }
#  }
#  rownames(merged)=merged[,1]
#  #colnames(merged)=colnames(merged[,-1])
#  #get rid of locus column
#  merged=merged[,-1]
# write.table(merged, "5StrainsEffCts.txt", sep="\t") 
#  rm(df,merged)




#   strain1.tx=x[,which(substr(colnames(x),1,3)==paste0(strain1,"T"))]
#   strain2.tx=x[,which(substr(colnames(x),1,3)==paste0(strain2,"T"))]
#   cbind(strain1.tx, strain2.tx,)  
#   
#   is.tx=which(colnames(x))
#   which(targets$strain==strain1)
#   
#   targets=data.frame(strain=substr(colnames(x),1,2),treatment=substr(colnames(x),3,3), row.names = colnames(x))
#   (targets$group = factor(paste(targets$strain, targets$treatment, sep=".")))
#   (targets)
#   
#   design <- model.matrix(~0+group)
#   colnames(design) <- levels(group)
#   fit <- glmFit(y, design)
#   
#   mycontrasts=makeContrasts(
#     cxvcx=B6.C-CA.C,
#     txvtx=B6.T-CA.T,
#     levels=design
#   )
#   
#   
#   control=paste0(samples,".control")
#   treated=paste0(samples,".treated")
#   tx=unique(targets$group[which(substr(targets$group,4,4)=="T")])
#   cx=unique(targets$group[which(substr(targets$group,4,4)=="C")])
#   
#   mycontrasts=makeContrasts(
#     assign(control,cx1-cx2),
#     assign(treated,tx1-tx2),
#     levels=design
#   )


# #subset the data and save each subset as an R object
# b6=data[,is.b6]
# save(b6, "B6-effcountdata.txt") #, sep=",", col.names = TRUE, row.names = TRUE)
# ca=data[,is.ca]
# write.table(b6, "CA-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# db=data[,is.db]
# write.table(b6, "DB-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# pk=data[,is.pk]
# write.table(b6, "PK-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# ws=data[,is.ws]
# write.table(b6, "WS-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# b6.ca=data[,c(is.b6,is.ca)]
# write.table(b6.ca, "B6vsCA-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# b6.db=data[,c(is.b6,is.db)]
# write.table(b6.db, "B6vsDB-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# b6.pk=data[,c(is.b6,is.pk)]
# write.table(b6.pk, "B6vsPK-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# b6.ws=data[,c(is.b6,is.ws)]
# write.table(b6.ws, "B6vsWS-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# ca.db=data[,c(is.ca,is.db)]
# write.table(ca.db, "CAvsDB-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# ca.pk=data[,c(is.ca,is.pk)]
# write.table(ca.pk, "CAvsPK-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# ca.ws=data[,c(is.ca,is.ws)]
# write.table(ca.ws, "CAvsWS-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# db.pk=data[,c(is.db,is.pk)]
# write.table(db.pk, "DBvsPK-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# db.ws=data[,c(is.db,is.ws)]
# write.table(db.ws, "DBvsWS-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)
# pk.ws=data[,c(is.pk,is.ws)]
# write.table(pk.ws, "PKvsWS-effcountdata.txt", sep=",", col.names = TRUE, row.names = TRUE)