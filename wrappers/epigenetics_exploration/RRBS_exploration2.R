#RRBS exploration
#This script has code for looking at correlations between DNA methylation
#and inbred expression in sliding windows and biological bins
#It also overlays DNA methylation data with histone modification data
#===============================================================
# Set Parameters
#===============================================================
start.feature = "start_position"
end.feature = "end_position"
upstream.buffer = 5000
downstream.buffer = 5000
T.or.C <- "C"
relative.chrom.state <- TRUE
calculate.summary.data <- TRUE
max.states = 6
state.colors <- c("blue", "green", "gray", "yellow", "red", "purple")
results.dir <- "~/Documents/Data/Epigenetics/Results/Exploration"
#===============================================================
# Load Functions
#===============================================================
r.dir <- "~/Documents/git_repositories/epigenetics"
setwd(r.dir)
all.fun <- list.files(pattern = "*.R")
for(i in 1:length(all.fun)){source(all.fun[i])}
source('~/Documents/git_repositories/useful_r_code/lapply_pb.R')

library(usefulScripts)
library(biomaRt)
library(e1071)
library(SNPtools)
library(gplot)
library(ape)
library(phytools)
library(gProfileR)
library(vioplot)
library(stringr)
library(usefulScripts)
source('~/Documents/git_repositories/epigenetics/get.gene.expr.R')
source('~/Documents/git_repositories/epigenetics/get.methyl.pos.R')
source('~/Documents/git_repositories/epigenetics/align.states.R')
source('~/Documents/git_repositories/useful_r_code/get.nearest.pt.R')

mus <- useEnsembl(biomart="ensembl", dataset="mmusculus_gene_ensembl")

#===============================================================
# Specify directories
#===============================================================
if(T.or.C == "T"){
data.dir <- "/Applications/ChromHMM/results/T/states_06"
	}else{
	data.dir <- "/Applications/ChromHMM/results/C/states_06"
	}


#===============================================================
# Load original expression table
#===============================================================
rna.seq <- as.matrix(read.table("~/Documents/Data/Epigenetics/Data/RNASeq/9StrainsEffCts.txt", header = TRUE, stringsAsFactors = FALSE, sep = "\t"))
# high.expr <- which(rowSums(rna.seq) > ncol(rna.seq))
# rna.seq <- rna.seq[high.expr,]




#===============================================================
# Get Chromatin state info
#===============================================================

#read in all bed files for the strains
#This is a bit of a nightmare because of 
#inconsistent file naming, but it works
#for now. Check by hand if changing files
#or names
setwd(data.dir)

bed.files <- list.files(pattern = "*dense.bed")
strains <- substr(colnames(rna.seq), 1,2)
strain.names <- sort(unique(strains))
strain.bed.names <- c("A_", "B6_", "CAST_", "DBA_", "NOD_", "NZO_", "PWK_", "129S1_", "WSB_")

all.bed <- vector(mode = "list", length = length(strain.names))
names(all.bed) <- strain.names
for(i in 1:length(strain.names)){
	cat("\n", i, "\n")
	bed.locale <- grep(pattern = strain.bed.names[i], bed.files)[1]
	if(!is.na(bed.locale) > 0){
		all.bed[[i]] <- read.table(bed.files[bed.locale], sep = "\t", skip = 1, stringsAsFactors = FALSE)
		}
	}
not.null.locale <- unlist(lapply(all.bed, function(x) !is.null(x)))
all.bed <- all.bed[not.null.locale]


#===============================================================
# Get gene info
#===============================================================

setwd(results.dir)
#get mean gene expression
mean.expr <- readRDS(paste("Mean_", T.or.C, "_Expression.RData", sep = ""))
group.mean.expr <- readRDS(paste("Group_Mean_", T.or.C, "_Expression.RData", sep = ""))
centered.expr <- lapply(group.mean.expr, function(x) x - mean(x))

gene.info.table <- readRDS("gene.info.table.RData")
gene.exon.table <- readRDS("gene.exon.table.RData")

#read in the chromatin states
filename <- paste("Chromatin_States_", start.feature, "_", end.feature, "_", upstream.buffer, "_", downstream.buffer, ".RData", sep = "")
chrom.mats <- readRDS(filename)

state.key <- read.table("/Applications/ChromHMM/results/C/states_06/state_key.txt", stringsAsFactors = FALSE, sep = "\t")

setwd("/Applications/ChromHMM/results/C/states_06")
col.table <- as.matrix(read.table("strain.color.table.txt", stringsAsFactors = FALSE, sep = "\t", comment.char = "&"))
col.table[1,1] <- "129S1"

#===============================================================
# Go to DNA methylation directory
#===============================================================
setwd("~/Documents/Data/Epigenetics/Data/DNA_methylation")

#I had to take out extra underscores in the file names for this naming structure to work
#I also called sample 3_2, 3.2

all.files <- list.files(pattern = ".bed")
split.names <- strsplit(all.files, "_")
split.table <- list2Matrix(split.names)
split.table[which(split.table[,4] == ""),4] <- "cont"

strain <- split.table[,3]
treat <- split.table[,4]
id <- split.table[,5]

id.table <- cbind(strain, treat, id)
id.table[,1] <- str_to_upper(id.table[,1])
ind.names <- apply(id.table, 1, function(x) paste(x[1], x[2], x[3], sep = "_"))

#all.data can be regenerated by code in RRBS_exploration.R
all.data <- readRDS("All.Methylation.RData")
t.locale <- grep("72hr", names(all.data))
c.locale <- setdiff(1:length(all.data), t.locale)
if(T.or.C == "T"){
	methyl.data <- all.data[t.locale]
	}else{
	methyl.data <- all.data[c.locale]
	}


# read in cis eQTL results
do.eqtl <- readRDS("~/Documents/Data/Epigenetics/DOQTL/DO478_tpm_20360_cis_allele_coef.RData")
qtl.ids <- lapply(do.eqtl, function(x) x[[1]][1,1])
is.missing <- unlist(lapply(qtl.ids, function(x) is.null(x)))
qtl.ids[is.missing] <- "NA"
qtl.id.v <- unlist(qtl.ids)

#object built in epigen.states.and.expr.R
# rnaseq.gene.info <- getBM(attributes = c("ensembl_gene_id", "external_gene_name", "chromosome_name", "start_position", "end_position", "transcription_start_site", "5_utr_start","5_utr_end","3_utr_start", "3_utr_end", "strand"), filters = "ensembl_gene_id", values = qtl.id.v, mart = mus)
# saveRDS(rnaseq.gene.info, "RNASeq_gene_info.RData")
rnaseq.gene.info <- readRDS("~/Documents/Data/Epigenetics/Results/Exploration/RNASeq_gene_info.RData")


gene.name = "Corin"
gene.name = "Gpr107"
gene.name = "Hsd3b1"
gene.name = "Fer"
gene.name = "Nnt"

upstream.buffer <- downstream.buffer <- 1e5

methyl.table <- get.methyl.table(gene.name, gene.info.table, methyl.data, upstream.buffer, downstream.buffer)

quartz(width = 8, height = 4)
plot.methyl.table(methyl.table, gene.name, gene.info.table, gene.exon.table, show.exons = TRUE)

quartz(width = 8, height = 6)
par(mfrow = c(2,1), mar = c(2,2,2,2))
smoothed.methyl <- plot.smooth.methyl.table(methyl.table, gene.name, 'coverage', 0.1, 0.005, col.table, gene.info.table)
gene.expr <- plot.expression(gene.name, gene.info.table, rna.seq, "C", col.table)

quartz();plot.one.gene(qtl.ids, do.eqtl, rnaseq.gene.info, rna.seq, gene.name, start.feature, end.feature, upstream.buffer, downstream.buffer, get.snps = FALSE)

# #test shifting the rows around a bit
# #These shifing values are for Fer
# shifted.table <- adjust.methyl.table(methyl.table, c(2:9), c(-29, 65, 120, -60, -90, 25, 80, 132))
# quartz();imageWithText(t(shifted.table), show.text = FALSE, row.names = colnames(methyl.table))

quartz()
methyl.bin.cor <- methylation_expression_window(smoothed.methyl, gene.expr, gene.name, col.table)

chrom.props <- state.window.expr(gene.name, gene.info.table, gene.exon.table, chrom.mats, group.mean.expr, state.key, window.gene.prop = 0.2, gap.prop = 0.01, bin.by = "window")

quartz()
par(xpd = FALSE)
plot.chrom.and.methyl.cor(chrom.props, methyl.bin.cor, state.colors, gene.name)
